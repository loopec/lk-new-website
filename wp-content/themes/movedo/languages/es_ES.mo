��    4      �  G   \      x     y     �  
   �     �     �     �     �     �     �     �     �     �                         -     >     V     ^     v     �     �  	   �     �     �     �     �     �     �  
   �     �          '     6     :  O   I  E   �     �     �     �     �          
          (  $   =     b  	   e  	   o     y     �     �
     �
     �
     �
     �
     �
     �
                5     I     O     c     }     �     �     �     �     �     �     �          
          %     +     2  *   J  
   u  
   �     �     �  !   �  
   �     �     �  L   �  `   I     �     �  	   �     �     �     �  
   �     �  -        F     I  	   Y     c     #   )         1                  "              +   $      &                           0       '   -       	             
   4   .   (                 ,         %             /             3      2   *                                               !    %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Home Hours Leave a Reply Leave a Reply to Link Relationship (XFN) Log out Log out of this account Logged in as Min Monthly Archives : Move down Move up Name Navigation Label Open link in a new window/tab Page %s Phone: Posts By : Search Results for : Search Results for : %s Search for ... Sec Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. Title Attribute URL Website Website: Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. in logged in read more to post a comment. Project-Id-Version: Movedo v2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-13 16:30+0200
PO-Revision-Date: 2018-02-13 16:30+0200
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
X-Poedit-SearchPathExcluded-1: includes/framework
X-Poedit-SearchPathExcluded-2: tribe-events
 %s (no válido) %s (Pendiente) (requerido) Todo Archivos Por: Clases CSS (opcional) Cancelar Respuesta Los comentarios están cerrados. Archivo por Días : Días Correo electrónico Editar elemento del Menú Inicio Horas Dejar una Respuesta Dejar una Respuesta a Relación de links (XFN) Finalizar la sesión Salir de esta cuenta Conectado como Min Archivos Mensuales : Bajar Subir Nombre Etiqueta de Navegación Abrir enlace en una nueva ventana/pestaña Página %s Teléfono: Entradas Por : Resultados de Búsqueda para : Resultados de Búsqueda para : %s Buscar ... Seg Enviar Comentario La descripción será desplegada en el menú si el tema corriente lo soporta Esta entrada está protegida con contraseña. Introduce la contraseña para ver los comentarios. Atributo Título URL Sitio web Pagina Web: Semanas Archivos Anuales : Tienes que Entra tu Comentario Aquí... Tu comentario está pendiente de moderación. en iniciar sesión leer más para publicar un comentario. 